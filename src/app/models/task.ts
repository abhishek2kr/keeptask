export class Task {
    taskId: number;
    taskTitle: string;
    taskDesc: string;
    taskIsDeleted: boolean;
    taskIsCompleted: boolean;
}