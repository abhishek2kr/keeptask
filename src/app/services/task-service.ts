import { Injectable } from '@angular/core';
import {Task} from '../models/task';
import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
 

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type': 'application/json; charset=UTF-8'
//   })
// }

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  taskList: Task[];
  constructor(private http : HttpClient) { }

  getTaskList():Observable<any>{
    const url = 'http://www.mocky.io/v2/5e91f3ce3300008a00e9d14f';
    return this.http.get<any>(url);
  }
}
