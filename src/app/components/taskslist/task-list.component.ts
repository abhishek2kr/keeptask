import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../services/task-service';
import {Task} from '../../models/task';
@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  
  taskList: Task[];
  constructor(private taskService: TaskService) { 
  }
  ngOnInit() {
     this.taskService.getTaskList().subscribe(taskList => this.taskList = taskList)
  }
 

}
